import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'ggt-berechnen',
  styleUrl: 'ggt-berechnen.css',
  shadow: false,
})
export class GgtBerechnen {
  @State() values: number[] = [];
  ggt: number = 0;

  handleInputValues(values: string) {
    this.values = values.split(' ')
      .map(value => parseInt(value))
      .filter(value => !isNaN(value));
  }

  calcGgt() {
    if (this.values.length > 1) {
      let currentGgt = this.values[0];
      for (let i = 0; this.values.length > i; i++) {
        currentGgt = calculateGgt(this.values[i], currentGgt);
      }
      return currentGgt;
    }
  }

  render() {
    return (

      <div>
        <input type='text' onInput={e => this.handleInputValues((e.target as any).value)}
               value={this.values.join(' ')} />
        <p>Zahlen bitte mit Leerzeichen getrennt eintragen.</p>
        <span class='bold'>ggT:</span> {this.calcGgt()}
      </div>
    );
  }

}

function calculateGgt(a: number, b: number): number {
  if (isNaN(a) || isNaN(b)) {
    return 0;
  }
  if (b === 0) {
    return a;
  }
  return calculateGgt(b, a % b);
}
