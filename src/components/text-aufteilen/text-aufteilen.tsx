import { Component, Host, h, State } from '@stencil/core';

@Component({
  tag: 'text-aufteilen',
  styleUrl: 'text-aufteilen.css',
  shadow: false,
})
export class TextAufteilen {

  @State() encryptedText = '';
  @State() keyLength = 1;

  handleInput(e) {
    this.encryptedText = (e as any).target.value;
  }

  splitText(): string[] {
    const splitText = [];
    for (let i = 0; i < this.keyLength; i++) {
      splitText[i] = '';
    }
    for (let i = 0; i < this.encryptedText.length; i++) {
      const indexText = i % this.keyLength;
      console.log(indexText);
      splitText[indexText] = splitText[indexText].concat(this.encryptedText.charAt(i));
    }

    console.log(splitText);
    return splitText;
  }

  render() {
    return (
      <Host>
        <textarea id={'text-input-aufteilen'} value={this.encryptedText}
                  onInput={e => this.handleInput(e as InputEvent)} />
        <p>Schlüssellänge:</p>
        <input type='number' min='1' onInput={e => this.keyLength = (e.target as any).value} value={this.keyLength} />
        {this.splitText().map((text, index) => <div><p class='bold'>Text {index}</p><p>{text}</p></div>)}
      </Host>
    );
  }

}
