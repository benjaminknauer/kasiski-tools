import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'kasiski-buchstaben-wiederholungen',
  styleUrl: 'kasiski-buchstaben-wiederholungen.css',
  shadow: false,
})
export class KasiskiBuchstabenWiederholungen {
  @State() min: number = 4;
  @State() max: number = 10;
  @State() encryptedText = '';


  searchRepeats(): Map<string, number> {
    const allChunks = new Set([]);
    console.log(this.max);
    for (let i = this.min; i <= this.max; i++) {
      Array.from(allChunkPermutations(this.encryptedText, i))
        .forEach(chunks => allChunks.add(chunks));
    }

    const allChunksWithDistance = new Map();

    Array.from(allChunks)
      .filter((elem) => testStringMultipleTimesInText(this.encryptedText, elem))
      .forEach(stringChunk => allChunksWithDistance.set(stringChunk, calculateDistance(this.encryptedText, stringChunk)));

    return allChunksWithDistance;
  }


  handleInput(event: InputEvent): void {
    this.encryptedText = (event.target as HTMLTextAreaElement).value;
  }

  render() {
    const repeats = this.searchRepeats();

    return (
      <div>
        <p class='range-field'>
          Min Länge: {this.min}
          <input type='range' id='min' min='2' max='9' value={this.min}
                 onInput={e => this.min = (e.target as any).value} />
        </p>
        <p class='range-field'>
          Max Länge: {this.max}
          <input type='range' id='max' min='2' max='9' value={this.max}
                 onInput={e => this.max = (e.target as any).value} />
        </p>
        <textarea id={'text-input'} value={this.encryptedText} onInput={e => this.handleInput(e as InputEvent)} />
        {repeats.size > 0 ?
          <table class='striped'>
            <thead>
            <tr>
              <th>Text</th>
              <th>Distanz</th>
            </tr>
            </thead>
            <tbody>
            {
              Array.from(repeats.keys()).map(string => {
                return <tr>
                  <td>{string}</td>
                  <td>{repeats.get(string)}</td>
                </tr>;
              })}
            </tbody>
          </table> : ''}
      </div>
    );
  }
}


function calculateDistance(text: string, searchString: string): number {
  // TODO findet nicht immer die kürzeste Distanz, sondern die Distanz von 1. und 2. Auftreten
  const firstIndex = text.indexOf(searchString);
  const nextIndex = text.indexOf(searchString, firstIndex + 1);

  return nextIndex - firstIndex;
}

function allChunkPermutations(input: string, chunkSize: number): Set<string> {
  const allChunks: Set<string> = new Set();
  for (let i = 0; i < chunkSize; i++) {
    const chunks = chunkStr(input.substring(i), chunkSize);
    chunks.forEach((chunk) => allChunks.add(chunk));
  }

  return allChunks;
}

function chunkStr(input: string, chunkSize: number): Set<string> {
  console.log(chunkSize);
  const chunks: Set<string> = new Set();
  for (let i = 0, charsLength = input.length; i < charsLength; i += chunkSize) {
    const chunk = input.substring(i, i + chunkSize);
    if (chunk.length === chunkSize) {
      chunks.add(chunk);
    }
  }

  return chunks;
}

function testStringMultipleTimesInText(text: string, testString: string): boolean {
  return countOccurences(text, testString) > 1;
}

function countOccurences(text: string, testString: string): number {
  return text.split(testString).length - 1;
}
